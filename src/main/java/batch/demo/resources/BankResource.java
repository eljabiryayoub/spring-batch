package batch.demo.resources;

import batch.demo.batch.BankTransactionItemAnalyticsProcessor;
import lombok.AllArgsConstructor;
import org.springframework.batch.core.*;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/batch")
@AllArgsConstructor
public class BankResource {
    private JobLauncher jobLauncher;
    private Job job;
    private BankTransactionItemAnalyticsProcessor bankTransactionItemAnalyticsProcessor;

    @GetMapping("/start")
    public BatchStatus load() throws Exception {

        Map<String, JobParameter> jobParameterMap = new HashMap<>();
        jobParameterMap.put("time", new JobParameter(System.currentTimeMillis()));
        JobExecution jobExecution = jobLauncher.run(job, new JobParameters(jobParameterMap));
        while (jobExecution.isRunning()) {
            System.out.println(".....");
        }
        return jobExecution.getStatus();
    }

    @GetMapping("/analytics")
    public Map<String, Double> analytics() {
        Map<String, Double> map = new HashMap<>();
        map.put("totalCredit", bankTransactionItemAnalyticsProcessor.getTotalCredit());
        map.put("totalDebit", bankTransactionItemAnalyticsProcessor.getTotalDebit());
        return map;

    }
}
