package batch.demo.configuration;

import batch.demo.batch.BankTransactionItemAnalyticsProcessor;
import batch.demo.batch.BankTransactionItemProcessor;
import batch.demo.model.BankTransaction;
import batch.demo.repository.BankTransactionRepository;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.LineMapper;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.batch.item.support.CompositeItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;

import java.util.ArrayList;
import java.util.List;


@EnableBatchProcessing
@Configuration
public class BatchConfig {
//    @Autowired
//    private BankTransactionRepository bankTransactionRepository;
    @Autowired
    private JobBuilderFactory jobBuilderFactory;
    @Autowired
    private StepBuilderFactory stepBuilderFactory;
    @Autowired
    private ItemReader<BankTransaction> bankTransactionItemReader;
    @Autowired
    private ItemWriter<BankTransaction> bankTransactionItemWriter;
//    @Autowired
//    private ItemProcessor<BankTransaction, BankTransaction> bankTransactionItemProcessor;

    @Bean
    public Job bankJob() {
        return this.jobBuilderFactory.get("bank-data-loader-job")
                .start(step())
                .build();

    }

    public Step step() {
        return this.stepBuilderFactory
                .get("step-load-data")
                .<BankTransaction, BankTransaction>chunk(100)
                .reader(bankTransactionItemReader)
                .processor(composIteItemProcessor())
                .writer(bankTransactionItemWriter)
                .build();
    }

    @Bean
    public ItemProcessor<BankTransaction, BankTransaction> composIteItemProcessor() {
        List<ItemProcessor<BankTransaction, BankTransaction>> itemProcessors = new ArrayList<>();
        itemProcessors.add(this.ItemAnalyticsProcessor());
        itemProcessors.add(this.ItemProcessor());
        CompositeItemProcessor<BankTransaction, BankTransaction> compositeItemProcessor = new CompositeItemProcessor<>();
        compositeItemProcessor.setDelegates(itemProcessors);
        return compositeItemProcessor;
    }

    @Bean
    public FlatFileItemReader<BankTransaction> flatFileItemReader(@Value("${inputFile}") Resource inputFile) {
        return new FlatFileItemReaderBuilder<BankTransaction>()
                .name("FFIR").linesToSkip(1)
                .lineMapper(lineMappe())
                .resource(inputFile)
                .build();
    }

    @Bean
    public LineMapper<BankTransaction> lineMappe() {
        DefaultLineMapper<BankTransaction> lineMapper = new DefaultLineMapper<>();
        DelimitedLineTokenizer lineTokenizer = new DelimitedLineTokenizer();
        lineTokenizer.setDelimiter(",");
        lineTokenizer.setStrict(false);
        lineTokenizer.setNames("id", "accountId", "strTransactionDate", "transactionType", "amount");
        lineMapper.setLineTokenizer(lineTokenizer);
        BeanWrapperFieldSetMapper beanWrapperFieldSetMapper = new BeanWrapperFieldSetMapper();
        beanWrapperFieldSetMapper.setTargetType(BankTransaction.class);
        lineMapper.setFieldSetMapper(beanWrapperFieldSetMapper);
        return lineMapper;
    }

    @Bean
    public ItemProcessor<BankTransaction, BankTransaction> ItemAnalyticsProcessor() {
        return new BankTransactionItemAnalyticsProcessor();
    }

    @Bean
    public ItemProcessor<BankTransaction, BankTransaction> ItemProcessor() {
        return new BankTransactionItemProcessor();
    }

}
