package batch.demo.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.stereotype.Component;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;
import java.util.Date;
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
public class BankTransaction {
    @Id
    private Long id;
    private Long accountId;
    private String transactionType;
    private Date transactionDate;
    private Double amount;
    @Transient
    private String strTransactionDate;
}
